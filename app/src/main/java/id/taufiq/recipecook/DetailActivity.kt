package id.taufiq.recipecook

import android.os.Bundle
import android.viewbinding.library.activity.viewBinding
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import id.taufiq.recipecook.databinding.ActivityDetailBinding
import id.taufiq.recipecook.model.Recipes

class DetailActivity : AppCompatActivity() {

    private val binding: ActivityDetailBinding by viewBinding()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val data = intent.getParcelableExtra<Recipes>("DATA")


        binding.apply {

            tvNamaMakananDetail.text = data?.name ?: "not available"
            tvBahanDetail.text = data?.bahan ?: "not available"
            tvCaraBuatDetail.text = data?.caraBuat ?: "not available"

            Glide.with(this@DetailActivity).apply {
                load(data?.image ?: "not available").into(ivMkananDetail)
            }

        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}