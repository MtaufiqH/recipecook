package id.taufiq.recipecook.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created By Taufiq on 12/15/2020.
 *
 */
@Parcelize
data class Recipes(
    val name: String,
    val bahan: String =
        """
1. 1 Siung bawang merah
2. 1 siung bawang putih
3. 1 kg daging
5. wortel
4. Jagung
6. penyedap rasa 
7. minyak goreng
8. kecap dan lombok
9. nasi putih
        """.trimMargin(),
    val caraBuat: String = """
        1. Masukan minyak goreng ke wajan 
        2. Tumis irisan bawang merah dan bawang putih ke wajan yang telah diberi minyak goreng
        3. setelah tumisan wangi, masukan potongan daging yang telah dimasak
        4. Masukkan juga potongan-potongan jagung beserta wortel
        5. setelah itu masukan nasi putih dan beri penyedap rasa
        6. berikan kecap dan lombok secukupnya
        7. aduk hingga nasi dan tumisan menyatu rata
        8. nasi goreng siap di sajikan. 
        
            """.trimIndent(),
    val image: String
) : Parcelable