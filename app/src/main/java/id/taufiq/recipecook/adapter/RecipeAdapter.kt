package id.taufiq.recipecook.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.taufiq.recipecook.R
import id.taufiq.recipecook.databinding.RecipeRowBinding
import id.taufiq.recipecook.model.Recipes


/**
 * Created By Taufiq on 12/15/2020.
 *
 */

class RecipeAdapter(private val context: Context, private val recipe: List<Recipes>, private val listener: (Recipes) -> Unit) :
    RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder>() {


    class RecipeViewHolder(private val item: RecipeRowBinding) :
        RecyclerView.ViewHolder(item.root) {
        fun binding(context: Context, data: Recipes, listener: (Recipes) -> Unit) {

            with(item) {
                tvRecipeName.text = data.name
                Glide.with(context).load(data.image).into(ivRecipe)
                icFavorite.setOnClickListener {
                    icFavorite.setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary))
                }
                icBookmark.setOnClickListener {
                    icBookmark.setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary))
                }

                icShare.setOnClickListener {
                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "text/plain"
                    intent.putExtra(Intent.EXTRA_TEXT, data.name)
                    startActivity(context, intent, null)

                }

                itemRow.setOnClickListener { listener(data) }


            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RecipeRowBinding.inflate(inflater, parent, false)
        return RecipeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        val item = recipe[position]
        holder.binding(context, item, listener)
    }

    override fun getItemCount(): Int = recipe.size
}