package id.taufiq.recipecook

import android.content.Intent
import android.os.Bundle
import android.viewbinding.library.activity.viewBinding
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import id.taufiq.recipecook.adapter.RecipeAdapter
import id.taufiq.recipecook.databinding.ActivityMainBinding
import id.taufiq.recipecook.model.Recipes

class MainActivity : AppCompatActivity() {

    private lateinit var listData: List<Recipes>
    private val binding: ActivityMainBinding by viewBinding()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val padding = resources.getDimensionPixelOffset(R.dimen.padding) / 4

        initData()


        binding.rvRecipe.apply {
            layoutManager =
                GridLayoutManager(this@MainActivity, 2, GridLayoutManager.VERTICAL, false)
            clipToPadding = false
            addItemDecoration(ItemDecorationRecipes(padding))
            adapter = RecipeAdapter(this@MainActivity, listData) { data ->
                startActivity(
                    Intent(this@MainActivity, DetailActivity::class.java)
                        .putExtra("DATA", data)
                )

            }
        }

    }


    private fun initData() {
        listData = ArrayList()
        listData = listOf(
            Recipes(
                name = "Sapi Lada Hitam",
                image = "https://www.masakapahariini.com/wp-content/uploads/2019/06/sapi-lada-hitam-400x240.jpg"
            ),
            Recipes(
                name = "Gulai nangka",
                image = "https://www.masakapahariini.com/wp-content/uploads/2020/04/Gulai-Nangka-400x240.jpg"
            ),
            Recipes(
                name = "Gado-gado",
                image = "https://www.masakapahariini.com/wp-content/uploads/2020/10/gado-gado-dinikmati-400x240.jpg"
            ),
            Recipes(
                name = "Pangsit",
                image = "https://www.masakapahariini.com/wp-content/uploads/2018/09/pangsit-kuah-MAHI-1-400x240.jpg"
            ),
            Recipes(
                name = "Samosa",
                image = "https://www.masakapahariini.com/wp-content/uploads/2020/11/samosa-400x240.jpg"
            ),
            Recipes(
                name = "Nasi Bogana",
                image = "https://www.masakapahariini.com/wp-content/uploads/2020/11/resep-nasi-bogana-400x240.jpg"
            ),
            Recipes(
                name = "Batagor Bandung",
                image = "https://www.masakapahariini.com/wp-content/uploads/2020/11/batagor-bandung-disajikan-400x240.jpg"
            ),
            Recipes(
                name = "Sate Jamur",
                image = "https://www.masakapahariini.com/wp-content/uploads/2019/12/sate-jamur-400x240.jpg"
            ),
            Recipes(
                name = "Ayam Kecap",
                image = "https://www.masakapahariini.com/wp-content/uploads/2018/04/resep-ayam-kecap-dinikmati-400x240.jpg"
            )
        )
    }
}