package id.taufiq.recipecook

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

/**
 * Created By Taufiq on 12/17/2020.
 *
 */
class ItemDecorationRecipes(private val itemOfset: Int) : ItemDecoration() {

    constructor(
        context: Context,
        itemOfset: Int
    ) : this(context.resources.getDimensionPixelOffset(itemOfset))


    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.set(itemOfset, itemOfset, itemOfset, itemOfset)
    }
}